const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');

const app = express();
const { API_VERSION } = require('./config')

// Load routings
const userRoutes = require('./routes/user')

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Configure Header HTTP
// ....
app.use(cors(/* corsOptions */))

// Router Basic
app.use(`/api/${API_VERSION}`, userRoutes)


module.exports = app;