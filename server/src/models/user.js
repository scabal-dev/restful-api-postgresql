//de momento no esta integrado**
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchame = Schema({
    name: String,
    lastname: String,
    email: {
        type: String,
        unique: true
    },
    phone: {
        type: String,
        unique: true
    },
    phoneStatus: Number,
    password: String,
    role: String,
    active: Boolean
});

module.exports = mongoose.model("User", UserSchame)