CREATE DATABASE wi-frio_develop;

CREATE TABLE users(
    id SERIAL PRIMARY KEY,
    name VARCHAR(40) NULL,
    lastname VARCHAR(40) NULL,
    gender VARCHAR(20) NULL,
    birthdate TIMESTAMP null,
    email VARCHAR(60) NULL,
    phone VARCHAR(15) NULL,
    created_on TIMESTAMP DEFAULT NOW(),
    last_login TIMESTAMP DEFAULT NOW()
);

CREATE TABLE email(
    id SERIAL PRIMARY KEY,
    name VARCHAR(40) NULL,
    email VARCHAR(60) NULL,
    title VARCHAR(60) NULL,
    message text NULL,
    created_on TIMESTAMP DEFAULT NOW(),
    last_login TIMESTAMP DEFAULT NOW()
);



INSERT INTO users (name,lastname,gender,birthdate,email,phone,created_on,last_login) VALUES
('Samuel','Cabal','Hombre','10/03/1998','samuelcabal.de@gmail.com','+524421333808', NOW(), NOW());