const app = require("./app");
const PORT_SERVER = process.env.PORT || 3977;
const { API_VERSION, API_HOST, pool } = require("./config");


pool.connect((error, result) => {
  if (error) {
    console.log(error);
  }else{
      console.log("La conexion a la base de datos es correcta.");

      app.listen(PORT_SERVER, () => {
        console.log("#####################");
        console.log("###### API REST #####");
        console.log("#####################");
        console.log(`http://${API_HOST}:${PORT_SERVER}/api/${API_VERSION}/`);
      });
  }
});
