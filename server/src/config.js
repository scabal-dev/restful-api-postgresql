const {Pool} = require("pg");

const API_VERSION = "v1";
const API_HOST = "localhost";
const DB_USER = "postgres";
const DB_PASSWORD = "Heineken21"
const PORT_DB = 5432;
const DB_NAME = "wifrio_develop";
const DB_DIALECT = "postgres";

const pool = new Pool({
  host: API_HOST,
  user: DB_USER,
  password: DB_PASSWORD,
  database: DB_NAME,
  port: PORT_DB
})

module.exports = {
  API_VERSION,
  API_HOST,
  DB_USER,
  DB_PASSWORD,
  PORT_DB,
  DB_NAME,
  DB_DIALECT,
  pool
};